package com.example.letoan.testfirebase;

/**
 * Created by LeToan on 4/18/2017.
 */

public class Model {
    private String title;
    private String image;
    private String describe;


    public Model(String title, String image, String describe) {
        this.title = title;
        this.image = image;
        this.describe = describe;
    }

    public Model() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
