package com.example.letoan.testfirebase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by LeToan on 4/18/2017.
 */

public class RecryAdapter extends RecyclerView.Adapter{

    private Context mContext;
    private ArrayList<Model> arrayList;

    public RecryAdapter(ArrayList<Model> arrayList, Context context){


        this.arrayList=arrayList;
        this.mContext=context;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view,parent,false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Model model=arrayList.get(position);
        ViewHolder myHolder= (ViewHolder) holder;
        myHolder.tvTitle.setText(model.getTitle());
        myHolder.tvDescription.setText(model.getDescribe());
        Picasso.with(mContext).load(model.getImage()).centerCrop().resize(50,50).into(myHolder.img);


    }

    public void setData(ArrayList<Model> mData){
        arrayList.clear();
        arrayList.addAll(mData);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        private TextView tvDescription;
        private TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            img= (ImageView) itemView.findViewById(R.id.img);
            tvDescription=(TextView)itemView.findViewById(R.id.tvDescription);
            tvTitle=(TextView)itemView.findViewById(R.id.tvTitle);
        }
    }

}
