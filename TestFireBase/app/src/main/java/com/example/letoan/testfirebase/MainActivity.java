package com.example.letoan.testfirebase;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private FirebaseDatabase database;
    private DatabaseReference myDATA;
    private RecyclerView myRecy;
    private RecryAdapter adapter;
    private ArrayList<Model> arrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database=FirebaseDatabase.getInstance();
        myDATA=database.getReference("workout");
        initRecy();
    }

    private void initRecy() {
        myRecy=(RecyclerView)findViewById(R.id.recycler_view);
        arrayList=new ArrayList<>();
        GridLayoutManager linearLayoutManager=new GridLayoutManager(getApplicationContext(),1);
        adapter=new RecryAdapter(arrayList,getApplicationContext());
        myRecy.setLayoutManager(linearLayoutManager);
        myRecy.setItemAnimator(new DefaultItemAnimator());
        myRecy.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataFromServer();
    }

    private void getDataFromServer() {
        myDATA.child("title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.e("toanle","dsadfdsfdsfdsfdsfdsfdsfdsfs");
                Model model=null;
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    model=data.getValue(Model.class);
                    arrayList.add(model);
                }
                adapter=new RecryAdapter(arrayList,getApplicationContext());
                myRecy.setAdapter(adapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




    }


}
